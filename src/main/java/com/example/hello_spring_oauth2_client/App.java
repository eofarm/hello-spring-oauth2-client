package com.example.hello_spring_oauth2_client;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
@EnableAutoConfiguration
@Configuration
@Controller
@EnableOAuth2Sso
public class App 
{
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    
    public static void main( String[] args )
    {
        SpringApplication.run(App.class, args);
    }
    
    @RequestMapping("/me")
    public String me(Authentication auth, Model model) 
    {
        return "me.html";
    }
    
    @RequestMapping({"/", "/index"})
    public String index() 
    {
        return "index.html";
    }
    
    @ModelAttribute("username")
    public String userName(OAuth2Authentication auth)
    {
        return auth.getName();
    }
    
    @ModelAttribute("userinfo")
    public Map<?,?> userIdentifier(OAuth2Authentication auth)
    {
        Authentication userAuth = auth.getUserAuthentication();
        return (Map<?,?>) userAuth.getDetails();
    }
}
