# README

## 1. Quickstart

### 1.1 Setup keystore

Create the server keystore at `config/server.jks` containing at least the following entries:

 * `server`: the private key entry to be used by this server for HTTPS communication
 * `root`: a trusted certificate to be used as a CA root verifying our peer (the identity server) 

### 1.2. Configure

Copy `src/main/resources/application.yml.example` to `src/main/resources/application.yml` and edit to adjusto to your needs.

### 1.3. Build and run

Run the application by providing our keystore as the JVM's trust store (so that `java.net` libraries can correctly pick up the CA root). For example:

    java -Djavax.net.ssl.trustStore=$PWD/config/server.jks -Djavax.net.ssl.trustStorePassword=secret \
        -jar target/hello-spring-oauth2-client-0.0.1-SNAPSHOT.jar
